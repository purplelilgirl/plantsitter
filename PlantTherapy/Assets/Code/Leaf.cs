﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using System.IO;

public class Leaf : MonoBehaviour
    
    //, IPointerEnterHandler, IPointerExitHandler
{
    public Image m_stem;
    public float m_stemFillAmount;

    float m_animTime = 5;
    float m_recordingTime = 60;

    [HideInInspector]
    public int m_currIdx;

    [HideInInspector]
    public AudioSource m_audioSource;

    /**
    public void OnPointerEnter(PointerEventData eventData)
    {   
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }
    **/
    private void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(PlayAudio);
    }

    public void Dead()
    {   LeanTween.cancel(gameObject);
        gameObject.GetComponent<Image>().color = new Color(0, 0, 0, 1);
    }

    public void Die()
    {   LeanTween.value(gameObject, FadeBlack, 1, 0, m_recordingTime*0.5f);
    }

    void FadeBlack(float value)
    {   gameObject.GetComponent<Image>().color = new Color(value, value, value, 1);
    }

    public void StopDying()
    {   LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, FadeBlack, gameObject.GetComponent<Image>().color.r, 1, m_recordingTime);
    }

    public void PlayAudio()
    {   StartCoroutine(GetAudioClip());
    }

    IEnumerator GetAudioClip()
    {
        #if UNITY_EDITOR
        var wavPath = Path.Combine(Directory.GetCurrentDirectory(), "Recording" + m_currIdx + ".wav");
        #else
        var wavPath = Path.Combine(Application.persistentDataPath, "Recording"+ m_currIdx +".wav");
        #endif

        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(wavPath, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {   Debug.Log(www.error);
            }
            else
            {   AudioClip clip = DownloadHandlerAudioClip.GetContent(www);
                m_audioSource.PlayOneShot(clip);
            }
        }
    }
}
