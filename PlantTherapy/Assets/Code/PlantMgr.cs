﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using NatMic;
using NatMic.Recorders;

public class PlantMgr : MonoBehaviour
{
    public GameObject m_water;

    public ScrollRect m_scrollRect;
    public RectTransform m_content;

    public Leaf[] m_leaves;
    private List<Leaf> m_leafList;

    public Transform m_moreStems;
    public Stem m_stemPrefab;

    public Text m_text1;
    public Text m_text2;
     
    private int m_currIdx;

    private float m_clickTime;
    private bool m_isClicked = false;

    public float m_animTime = 1;
    public float m_leafAnimTime = 0.5f;

    private System.DateTime m_currDateTime;

    public AudioSource m_audioSource;
    private IAudioDevice m_audioDevice;
    private WAVRecorder m_audioRecorder;

    private void Start()
    {
        m_text1.text = "How are you feeling today?";
        m_text2.text = "Talk to your Plant Therapist (not licensed)";

        m_water.SetActive(false);

        m_currIdx = PlayerPrefs.GetInt("CurrIdx", -1);

        m_leafList = new List<Leaf>();

        for (int i=0; i < m_leaves.Length; i++)
        {   m_leafList.Add(m_leaves[i]);
        }

        CreateStemPrefab();
        ShowLeafPrefab();

        m_currDateTime = System.DateTime.Now;
        
        string prevTimeStr = PlayerPrefs.GetString("PrevTime", "");

        if (prevTimeStr != "")
        {
            System.DateTime prevTime = System.DateTime.ParseExact(prevTimeStr, "yyyy-MM-dd HH:mm tt", System.Globalization.CultureInfo.InvariantCulture);

            System.TimeSpan ts = prevTime - m_currDateTime;

            // if more than 24 hours, plants die
            if (ts.TotalHours > 24)
            {   for (int i = 0; i < m_leafList.Count; i++)
                {   m_leafList[i].Dead();
                }
            }
        }

        PlayerPrefs.SetString("PrevTime", m_currDateTime.ToString("yyyy-MM-dd HH:mm tt"));

        if (m_currIdx < 15)
        {   m_scrollRect.horizontal = false;
        }   else
        {   m_scrollRect.horizontal = true;
        }

        m_audioDevice = AudioDevice.GetDevices()[0];
    }

    public void StartRecording()
    {   
        if (!m_audioDevice.IsRecording)
        {
            // Create a WAV recorder to record the audio to a file
            #if UNITY_EDITOR
            var wavPath = Path.Combine(Directory.GetCurrentDirectory(), "Recording"+m_currIdx+".wav");
            #else
            var wavPath = Path.Combine(Application.persistentDataPath, "Recording"+m_currIdx+".wav");
            #endif
            
            m_audioRecorder = new WAVRecorder(wavPath);
            m_audioDevice.StartRecording(44100, 1, m_audioRecorder);
        }
    }

    public void StopRecording()
    {
        if (m_audioDevice != null && m_audioDevice.IsRecording)
        {
            // Stop recording
            m_audioDevice.StopRecording();
            // Dispose the recorder
            m_audioRecorder.Dispose();
            Debug.Log("Saved recording to: " + m_audioRecorder.FilePath);
            // Play the recording in the scene
            //Application.OpenURL(audioRecorder.FilePath);
        }
    }

    void CreateStemPrefab()
    {
        if (m_currIdx > m_leafList.Count)
        {   GameObject stemPrefab = Instantiate(m_stemPrefab.gameObject, m_moreStems) as GameObject;
            m_leafList.AddRange(stemPrefab.GetComponent<Stem>().m_leaves);

            m_content.sizeDelta = new Vector2(m_content.sizeDelta.x+800, m_content.sizeDelta.y);

            LeanTween.value(gameObject, Scroll, m_scrollRect.horizontalNormalizedPosition, 1, m_leafAnimTime);

            CreateStemPrefab();
        }
        else
        {   return;
        }
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {   m_clickTime = Time.time;
            m_isClicked = true;
        }

        if (Input.GetMouseButtonUp(0))
        {   m_isClicked = false;

            if (m_water.activeInHierarchy)
            {
                StopRecording();

                m_water.SetActive(false);
                m_leafList[m_currIdx].gameObject.SetActive(true);
                LeanTween.value(gameObject, ShowLeaf, 0, 1, m_leafAnimTime);

                m_text1.text = "Thank you for sharing your feelings!";
                m_text2.text = "Come back again tomorrow";

                
            }
        }

        if (m_isClicked)
        {
            // if clicked longer than 0.5f second
            if (Time.time - m_clickTime > 0.5f)
            {
                m_currIdx++;
                PlayerPrefs.SetInt("CurrIdx", m_currIdx);

                StartRecording();

                if (m_currIdx >= 15)
                {   m_scrollRect.horizontal = true;
                }

                if (m_currIdx < m_leafList.Count)
                {   ShowWater();
                }   else
                {   Debug.Log("need to make more leaves");
                    CreateStemPrefab();
                    ShowLeafPrefab();
                    ShowWater();
                }

                for (int i = 0; i < m_leafList.Count; i++)
                {   m_leafList[i].StopDying();
                }
            }
        }

        if(m_water.activeInHierarchy)
        {   m_water.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
        }
    }

    void ShowLeafPrefab()
    {
        for (int i = 0; i < m_leafList.Count; i++)
        {
            m_leafList[i].m_currIdx = i;
            m_leafList[i].m_audioSource = m_audioSource;
            m_leafList[i].Die();
            
            if (i < m_currIdx)
            {
                Debug.Log("show leaf");

                m_leafList[i].gameObject.SetActive(true);
                m_leafList[i].m_stem.gameObject.SetActive(true);
                m_leafList[i].m_stem.fillAmount = m_leafList[i].m_stemFillAmount;
            }
            else
            {
                m_leafList[i].gameObject.SetActive(false);
                //m_leafList[i].m_stem.gameObject.SetActive(false);
            }
        }
    }

    void ShowWater()
    {
        m_leafList[m_currIdx].GetComponent<Image>().fillAmount = 0;
        m_leafList[m_currIdx].gameObject.SetActive(true);

        m_water.SetActive(true);
        m_isClicked = false;

        if (m_currIdx == 0)
        {   m_leafList[0].m_stem.fillAmount = 0;
        }
        else
        {
            if (m_leafList[m_currIdx].m_stem == m_leafList[m_currIdx - 1].m_stem)
            {   m_leafList[m_currIdx].m_stem.fillAmount = m_leafList[m_currIdx - 1].m_stemFillAmount;
            }   else
            {   m_leafList[m_currIdx].m_stem.fillAmount = 0;
            }
        }

        m_leafList[m_currIdx].m_stem.gameObject.SetActive(true);
        LeanTween.value(gameObject, ShowStem, m_leafList[m_currIdx].m_stem.fillAmount, m_leafList[m_currIdx].m_stemFillAmount, m_animTime);

    }

    void ShowStem(float value)
    {   m_leafList[m_currIdx].m_stem.fillAmount = value;
    }

    void ShowLeaf(float value)
    {   m_leafList[m_currIdx].GetComponent<Image>().fillAmount = value;
    }

    void Scroll(float value)
    {
        m_scrollRect.horizontalNormalizedPosition = value;
    }
}
